# Hackintosh Tools



## Table of contents

1. [ProperTree](#propertree)
2. [Credits](#credits)
3. [Table of references](#table-of-references)



## [ProperTree](https://github.com/corpnewt/ProperTree)



![ProperTree Launcher](./ProperTree/proper_tree_launcher.png)

### General

>  Cross platform GUI plist editor written in python.

### Installation

1. Clone or Download [ProperTree](https://github.com/corpnewt/ProperTree)

2. Go into `ProperTree.command` and change `python` to `python3` on line 1

3. Install `Tkinter` for python3

   ```bash
   sudo apt-get install python3-tk
   ```

4. Move the ProperTree repository to `/opt/`

   ```bash
   sudo mv ./ProperTree/ /opt/
   ```

5. (Optional) Create a symbolic link to `/usr/bin`

   ```bash
   sudo ln -s /opt/ProperTree/ProperTree.command /usr/bin/ProperTree.command
   ```

6. (Optional) Create a launcher icon

   - (Optional) if you want to use the custom icon move `favicon.png` to `/opt/ProperTree/`

     ```bash
     sudo mv ./favicon.png /opt/ProperTree/
     ```

   - move `ProperTree.desktop` to your `/usr/share/applications/` directory

     ```bash
     sudo mv ProperTree.desktop /usr/share/applications/
     ```



## Credits

- Thanks to [corpnewt](https://github.com/corpnewt) for [ProperTree](https://github.com/corpnewt/ProperTree)



## Table of references

1. [ProperTree](https://github.com/corpnewt/ProperTree)
2. [gibMacOS](https://github.com/corpnewt/gibMacOS)
3. [How can I create launchers on my desktop?](https://askubuntu.com/questions/64222/how-can-i-create-launchers-on-my-desktop)

